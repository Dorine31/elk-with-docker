# ElasticSearch filebeat Logstash Kibana

## Deploy with docker compose

```
docker compose up -d
docker ps
docker compose start
docker compose stop
docker compose down
```
## Create dashboards with kibana
http://localhost:5601/   